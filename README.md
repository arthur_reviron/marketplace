# marketplace

> A Vue.js project

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report

# run unit tests
npm run unit

# run e2e tests
npm run e2e

# run all tests
npm test
```

## Project Demo with docker
** IMPORTANT: Avant de pouvoir builder le projet, il faut avoir installé Docker et s'assurer de la présence de la commande `docker` dans le PATH **

après avoir cloné le projet, naviguez à la racine de celui-ci et exécutez les commandes suivantes pour build l'application.
``` bash
# Construction de l'image suivant les instructions du Dockerfile:
docker build -t vuejs-cookbook/dockerize-vuejs-app .

# Lancement de l'app dans un container Docker:
docker run -it -p 8080:8080 --rm --name dockerize-vuejs-marketplace vuejs-cookbook/dockerize-vuejs-app
```